import os
import sys
import json
import hashlib
import argparse
from pathlib import Path
from collections import defaultdict as ddict
from difflib import SequenceMatcher, HtmlDiff

from sciunit2 import workspace
from sciunit2.command.context import CheckoutContext
from sciunit2.util import Chdir


BUF_SIZE = 65536


def hash_file(f, cache):
    if 'hash_file' not in cache:
        cache['hash_file'] = {}
    fcache = cache['hash_file']
    if not Path(f).is_file():
        raise ValueError(f'{f} Not a File')
        fcache[f] = 'x'
    if f not in fcache:
        try:
            sha = hashlib.sha256()

            with open(f, 'rb') as fp:
                while True:
                    data = fp.read(BUF_SIZE)
                    if not data:
                        break
                    sha.update(data)

            fcache[f] = sha.hexdigest()
        except (Exception, KeyboardInterrupt):
            raise ValueError(f'Hashing Failed/Interrupted for "{f}"')
    return fcache[f]


def par_compare(syscall, par, cache):
    if syscall in ['EXECVE', 'EXECVE2', 'SPAWN']:
        return par[1:]
    elif syscall in ['READ', 'WRITE', 'READ-WRITE', 'CLOSE']:
        return par + [hash_file(os.path.join('cde-root', par[-1][1:]), cache)]
    elif syscall in ['SLEEP', 'EXIT', 'PTRACE']:
        return par
    else:
        raise NotImplementedError(f'{syscall} comparison Not Implemented')


def read_log_file(log, cache):
    log_d = ddict(list)
    for line in log:
        line = line.strip()
        if line[0] == '#':
            continue
        ts, pid, syscall, *par = line.split()
        if syscall in ['MEM']:
            continue
        try:
            log_d[pid].append(' '.join([syscall, *par_compare(syscall, par, cache)]))
        except ValueError:
            pass
    return log_d


def read_log(ex):
    cache = {}
    try:
        with open(f'{workspace.current()[1].location}/{ex}_hashes.json') as hf:
            hs = json.load(hf)
            cache['hash_file'] = {f[12:]: hs[f] for f in hs}
    except:
        print(f'Error opening hash file "{workspace.current()[1].location}/{ex}_hashes.json"')
    try:
        with open(f'{workspace.current()[1].location}/{ex}.log') as log:
            return read_log_file(log, cache)
    except:
        print(f'Error opening log file "{workspace.current()[1].location}/{ex}.log"')
    with CheckoutContext(ex) as (pkgdir, _):
        with Chdir(pkgdir):
            with open('provenance.cde-root.1.log') as log:
                return read_log_file(log, cache)



def exec_abs(e1, e2):
    e1l = read_log(e1)
    e2l = read_log(e2)

    ratios = []
    for i, (p1, p2) in enumerate(zip(e1l, e2l)):
        diff = SequenceMatcher(a=e1l[p1], b=e2l[p2])
        ratios.append(diff.ratio())
        html = HtmlDiff().make_file(e1l[p1], e2l[p2], e1, e2, True)
        with open(f'{i}.html', 'w') as html_file:
            html_file.write(html)

    print(ratios)



def main():

    parser = argparse.ArgumentParser(description='SciUnit Execution Abstraction for 2 SciUnit Executions')

    parser.add_argument('e1', type=str, help='First SciUnit Execution')
    parser.add_argument('e2', type=str, help='Second SciUnit Execution')

    args = parser.parse_args()

    exec_abs(**vars(args))


if __name__ == '__main__':
    main()
