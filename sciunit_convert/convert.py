"""
This module translates .py files to .ipynb and vice versa
"""
import os
import sys
import json
import argparse
import tempfile
import ast

# Path to directory
HERE = os.path.abspath(os.path.dirname(__file__))

TRIPLE_QUOTES = ["\"\"\"", "\'\'\'"]
SPACE = "{:<1}".format("")


def p2j(input_filename):
    # Check if source file exists and read
    try:
        with open(input_filename, 'r', encoding='utf-8') as infile:
            data = [l.rstrip('\n') for l in infile]
    except FileNotFoundError:
        print("Input file not found. Specify a valid input file.", file=sys.stderr)
        return

    # Read JSON files for .ipynb template
    with open(HERE + '/templates/cell_code.json', encoding='utf-8') as file:
        CODE = json.load(file)
    with open(HERE + '/templates/cell_markdown.json', encoding='utf-8') as file:
        MARKDOWN = json.load(file)
    with open(HERE + '/templates/metadata.json', encoding='utf-8') as file:
        MISC = json.load(file)

    # Initialise variables
    final = {}              # the dictionary/json of the final notebook
    cells = []              # an array of all markdown and code cells
    arr = []                # an array to store individual lines for a cell
    num_lines = len(data)   # no. of lines of code

    # Initialise variables for checks
    is_block_comment = False
    is_running_code = False
    is_running_comment = False
    next_is_code = False
    next_is_nothing = False
    next_is_function = False

    # Read source code line by line
    for i, line in enumerate(data):

        # Skip if line is empty
        if line == "":
            continue

        buffer = ""

        # Labels for current line
        contains_triple_quotes = TRIPLE_QUOTES[0] in line or TRIPLE_QUOTES[1] in line
        is_code = line.startswith("# pylint") or line.startswith("#pylint") or \
            line.startswith("#!") or line.startswith("# -*- coding") or \
            line.startswith("# coding=") or line.startswith("##") or \
            line.startswith("# FIXME") or line.startswith("#FIXME") or \
            line.startswith("# TODO") or line.startswith("#TODO") or \
            line.startswith("# This Python file uses the following encoding:")
        is_end_of_code = i == num_lines-1
        starts_with_hash = line.startswith("#")

        # Labels for next line
        try:
            next_is_code = not data[i+1].startswith("#")
        except IndexError:
            pass
        try:
            next_is_nothing = data[i+1] == ""
        except IndexError:
            pass
        try:
            next_is_function = False
            k = 1
            while i + k < len(data):
                if data[i + k] == '':
                    k += 1
                    continue
                if data[i + k].startswith(SPACE):
                    next_is_function = True
                break
            # next_is_function = data[i+1].startswith(SPACE) or (
            #     next_is_nothing and data[i+2].startswith(SPACE))
        except IndexError:
            pass

        # Sub-paragraph is a comment but not a running code
        if not is_running_code and (is_running_comment or
                                    (starts_with_hash and not is_code) or
                                    contains_triple_quotes):

            if contains_triple_quotes:
                is_block_comment = not is_block_comment

            buffer = line.replace(TRIPLE_QUOTES[0], "\n").\
                replace(TRIPLE_QUOTES[1], "\n")

            if not is_block_comment:
                if len(buffer) > 1:
                    buffer = buffer[2:] if buffer[1].isspace() else buffer[1:]
                else:
                    buffer = ""

            # Wrap this sub-paragraph as a markdown cell if
            # next line is end of code OR
            # (next line is a code but not a block comment) OR
            # (next line is nothing but not a block comment)
            if is_end_of_code or (next_is_code and not is_block_comment) or \
                    (next_is_nothing and not is_block_comment):
                arr.append("{}".format(buffer))
                MARKDOWN["source"] = arr
                cells.append(dict(MARKDOWN))
                arr = []
                is_running_comment = False
            else:
                buffer = buffer + "<br>\n"
                arr.append("{}".format(buffer))
                is_running_comment = True
                continue
        else:  # Sub-paragraph is a comment but not a running code
            buffer = line + '\n'

            # Wrap this sub-paragraph as a code cell if
            # (next line is end of code OR next line is nothing) AND NOT
            # (next line is nothing AND next line is part of a function)
            if (is_end_of_code or next_is_nothing) and not (next_is_nothing and next_is_function):
                arr.append("{}".format(buffer))
                CODE["source"] = arr
                cells.append(dict(CODE))
                arr = []
                is_running_code = False
            else:

                # Put another newline character if in a function
                try:
                    if data[i+1] == "" and (data[i+2].startswith("    #") or
                                            data[i+2].startswith("        #") or
                                            data[i+2].startswith("            #")):
                        buffer = buffer + "\n"
                except IndexError:
                    pass

                arr.append("{}".format(buffer))
                is_running_code = True
                continue

    # Finalise the contents of notebook
    final["cells"] = cells
    final.update(MISC)

    return final


def j2p(notebook, output_filename):
    final = [''.join(["# " + line.lstrip() for line in cell["source"] if not line.strip() == ""])
             if cell["cell_type"] == "markdown" else ''.join(cell["source"])
             for cell in notebook['cells']]
    final = '\n\n'.join(final)
    final = final.replace("<br>", "")

    with open(output_filename, "w", encoding='utf-8') as outfile:
        outfile.write(final)
        print("Python script {} written.".format(output_filename))


def add_hooks(notebook):

    code_cell = None
    for i, cell in enumerate(notebook['cells']):
        if cell["cell_type"] == "code":
            if code_cell is None:
                code_cell = i
            if isinstance(cell['source'], str):
                cell['source'] = [cell['source']]
            for j, code in enumerate(cell['source']):
                if code and code[0] in ['%']:
                    cell['source'][j] = '#' + code
            source = repr(''.join(cell['source']))
            cell['source'].extend(['\n', f'sciunit_convert_syscall.cell({source})'])
            

    if code_cell is not None:
        add_index = 0
        for i, line in enumerate(notebook['cells'][code_cell]['source']):
            try:
                ap = ast.parse(line)
                if ap.body and isinstance(ap.body[0], ast.ImportFrom) and ap.body[0].module == '__future__':
                    add_index = i + 1
            except SyntaxError: 
                break
        if add_index == len(notebook['cells'][code_cell]['source']):
            notebook['cells'][code_cell]['source'][-1] += '\n'
            notebook['cells'][code_cell]['source'].append('from sciunit_convert import syscall as sciunit_convert_syscall')
        else:
            notebook['cells'][code_cell]['source'].insert(add_index, 'from sciunit_convert import syscall as sciunit_convert_syscall\n')


def _check_files(input_filenames, output_directory):
    input_files, exts, output_filenames = [], [], []
    for input_filename in input_filenames:
        file_base = os.path.splitext(input_filename)[0]
        file_ext = os.path.splitext(input_filename)[1]
        if output_directory is None:
            output_filename = file_base + '_converted.py'
        else:
            try:
                os.mkdir(output_directory)
            except: pass
            output_filename = os.path.join(output_directory, os.path.split(file_base)[1] + '_converted.py')
        input_files.append(input_filename)
        exts.append(file_ext)
        output_filenames.append(output_filename)
    return input_files, exts, output_filenames


def main():
    """Parse arguments and perform file checking"""

    # Get input and output filenames
    parser = argparse.ArgumentParser(
        description="Convert Python scripts or notebooks to Python scripts with calls in between cells",
        usage=f'{sys.argv[0]} input_filenames.py/ipynb')
    parser.add_argument('input_filenames', nargs='+',
                        help='Python scripts/notebooks to parse')
    parser.add_argument('-o', '--output_directory',
                        help="Output Directory of Python scripts. If not specified, " +
                        "it will use the same directory as input")

    args = parser.parse_args()

    input_filenames, exts, output_filenames = _check_files(args.input_filenames, args.output_directory)

    for input_filename, ext, output_filename in zip(input_filenames, exts, output_filenames):
        if ext in ['.py']:
            notebook = p2j(input_filename)
        elif ext in ['.ipynb']:
            try:
                with open(input_filename, 'r', encoding='utf-8') as infile:
                    notebook = json.load(infile)
            except FileNotFoundError:
                print("Input file not found. Specify a valid input file.", file=sys.stderr)
                continue
        else:
            print(f'Invalid Extension `{ext}`', file=sys.stderr)
            continue

        add_hooks(notebook)

        j2p(notebook, output_filename)


if __name__ == "__main__":
    main()
