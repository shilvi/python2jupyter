import ctypes
import timeit
import os
import sys
import psutil
import pickle


TEMP_FILE = '.temp.bin'

libc = ctypes.CDLL("libc.so.6")
cells = [sys.argv]

prev_time = timeit.default_timer()
def cell(code):
    global prev_time
    c = timeit.default_timer() - prev_time
    s = psutil.Process(os.getpid()).memory_info().rss
    cells.append((code, c, s))
    pickle.dump(cells, open(os.path.abspath(TEMP_FILE), 'wb'))
    libc.ptrace(-2, 0, 0, 0)
    prev_time = timeit.default_timer()
