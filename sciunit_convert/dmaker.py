import argparse
from logging import shutdown
import os
import re
import json
from pathlib import Path
from collections import defaultdict as ddict

from sciunit2 import workspace
from sciunit2.command.context import CheckoutContext


def dmaker(D, execs):
    D.mkdir()
    hashes = ddict(list)
    files_rmap = ddict(lambda: ddict(list))
    for e in execs:
        for f, hash in json.load(open(f'{workspace.current()[1].location}/e{e}_hashes.json')).items():
            fs = f.split(os.sep)
            fpath = os.sep.join(fs[2:])
            if fs[1] == 'cde-root':
                hashes[fpath].append((e, hash))
                files_rmap[fpath][hash].append(e)
    individual = ddict(list)
    common = set()
    for f, f_hashes in hashes.items():
        if len(f_hashes) < len(execs) or len(set(h for _, h in f_hashes)) > 1:
            for e, _ in f_hashes:
                individual[e].append(f)
        else:
            common.add(f)
    with CheckoutContext(f'e{execs[0]}') as (pkgdir, _):
        (Path(pkgdir) / 'cde-root').rename(D / 'root')
    for f in individual[execs[0]]:
        (D / f'e{execs[0]}' / f).parent.mkdir(parents=True, exist_ok=True)
        (D / 'root' / f).rename(D / f'e{execs[0]}' / f)
    for e in execs[1:]:
        with CheckoutContext(f'e{e}') as (pkgdir, _):
            for f in individual[e]:
                (D / f'e{e}' / f).parent.mkdir(parents=True, exist_ok=True)
                (Path(pkgdir) / 'cde-root' / f).rename(D / f'e{e}' / f)
    for e in execs:
        for f in common:
            fp = D / 'vroots' / f'e{e}' / f
            fp.parent.mkdir(parents=True, exist_ok=True)
            fp.symlink_to(Path(os.path.relpath(D, fp.parent)) / 'root' / f)
        for f in individual[e]:
            fp = D / 'vroots' / f'e{e}' / f
            fp.parent.mkdir(parents=True, exist_ok=True)
            fp.symlink_to(Path(os.path.relpath(D, fp.parent)) / f'e{e}' / f)
    with open(D / 'lookup_table.json', 'w') as files_rmap_fp:
        json.dump(files_rmap, files_rmap_fp, indent=4, sort_keys=True)


def D_type(D_str):
    D = Path(D_str)
    if not D.parent.exists():
        raise argparse.ArgumentTypeError(f'Parent Directory "{D.parent}" does not exist')
    if D.exists():
        raise argparse.ArgumentTypeError(f'Directory "{D}" already exists')
    return D

def execs_type(execs_str):
    execs = set()
    for a in execs_str.split(','):
        match = re.match(r'e(\d+)(?:\-e(\d+))?', a)
        if not match:
            raise argparse.ArgumentTypeError('Invalid Representation. Example Usage: `e1-e3,e5-e6,e8,e10,...`')
        if match[2] is not None:
            execs.update(range(int(match[1]), int(match[2]) + 1))
        else:
            execs.add(int(match[1]))
    return sorted(execs)

def main():

    parser = argparse.ArgumentParser(description='Creates a Directory `D` of given executions')

    parser.add_argument('D', help='Output Directory, must not exist', type=D_type)

    parser.add_argument('execs', help='Executions in the form of `e[m1]-e[n1],e[n2],e[m3]-e[n3],...`', type=execs_type)

    args = parser.parse_args()

    dmaker(**vars(args))


if __name__ == '__main__':
    main()
