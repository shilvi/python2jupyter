import pickle as pkl


def is_cell_same(cell1, cell2):
    if cell1.code != cell2.code: return False
    # if abs(cell1.c - cell2.c) / max(cell1.c, cell2.c) > .5: return False
    # if abs(cell1.s - cell2.s) / max(cell1.s, cell2.s) > .5: return False
    if cell1.log != cell2.log: return False
    return True


class TreeNode:
    def __init__(self, code, c, s, log):
        self.count = 1
        self.code = code
        self.c = c
        self.s = s
        self.log = log
        self.children = []
    @classmethod
    def root(cls):
        return cls('', 0, float('inf'), '')
    def update(self, new_node):
        self.c = (self.c * self.count + new_node.c * new_node.count) / (self.count + new_node.count)
        self.s = (self.s * self.count + new_node.s * new_node.count) / (self.count + new_node.count)
        self.count += new_node.count
        self.children.extend(new_node.children)

class Tree:
    def __init__(self):
        self.root = TreeNode.root()
        self.reset(self.root)

    def reset(self, node=None):
        if node is None:
            temp = self.node
            self.node = self.root
            return temp
        else:
            self.node = node

    def add_child(self, code, c, s, log):
        new_node = TreeNode(code, c, s, log)
        for child in self.node.children:
            if is_cell_same(child, new_node):
                self.node = child
                self.node.update(new_node)
                break
        else:
            self.node.children.append(new_node)
            self.node = new_node



def save_tree(tree, filename):
    node = tree.reset()
    pkl.dump(tree, open(filename, 'wb'))
    tree.reset(node)

def get_tree(filename):
    try:
        return pkl.load(open(filename, 'rb'))
    except:
        return Tree()

def compute_cost(tree):
    def dfs(node):
        return node.c * node.count + sum(dfs(child) for child in node.children)
    return dfs(tree.root)
