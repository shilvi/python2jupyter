import argparse
import subprocess
import uuid
import pickle
import json
from itertools import groupby
from timeit import default_timer as timer

from sciunit2 import workspace

from sciunit_convert.tree import Tree, save_tree
from sciunit_convert.exec_abs import read_log
from sciunit_convert.syscall import TEMP_FILE


TREE_FILE = 'tree.bin'


def split_log(log, n, file_name):
    for pid in log:
        full_log = []
        cell_log = []
        for l in log[pid]:
            if 'PTRACE' in l:
                full_log.append(cell_log)
                cell_log = []
            elif file_name not in l:
                cell_log.append(l)
        if len(full_log) == n:
            return full_log
    raise ValueError(f'No Logs for {n} Cells')


def tree_update(tree, e, cells_file):
    start = timer()
    argv, *cells = pickle.load(open(cells_file, 'rb'))
    log = split_log(read_log(f'e{e}'), len(cells), argv[0])
    for c, l in zip(cells, log):
        tree.add_child(*c, l)
    tree.reset()
    end = timer()
    f = f'{workspace.current()[1].location}/e{e}_times.json'
    json.dump({**json.load(open(f)), 'tree': end - start}, open(f, 'w'))


def nbrunner(nbs):
    sciunit_workspace = 'sciunit_convert_' + uuid.uuid4().hex
    print(f'Creating New SciUnit Workspace `{sciunit_workspace}`')
    subprocess.run(['sciunit', 'create', sciunit_workspace])
    t = Tree()
    for i, nb in enumerate(nbs):
        print(f'Running {nb}')
        subprocess.run(['sciunit', 'exec', 'python', nb])
        tree_update(t, i + 1, TEMP_FILE)
    save_tree(t, TREE_FILE)


def main():

    parser = argparse.ArgumentParser(description='Run Notebooks with SciUnit and Generate Tree')

    parser.add_argument('nbs', nargs='+', help='Python Files to Run')

    args = parser.parse_args()

    nbrunner(**vars(args))


if __name__ == '__main__':
    main()
