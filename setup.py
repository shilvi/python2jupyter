#!/usr/bin/env python

import io
import os
from setuptools import setup

# Package meta-data.
VERSION = "1.0.0"
DESCRIPTION = "sciunit_convert: Convert a Python script to Jupyter notebook"

# Import the README and use it as the long-description.
# Note: this will only work if 'README.md' is present in your MANIFEST.in file!
HERE = os.path.abspath(os.path.dirname(__file__))
try:
    with io.open(os.path.join(HERE, 'README.md'), encoding='utf-8') as f:
        LONG_DESCRIPTION = '\n' + f.read()
except FileNotFoundError:
    LONG_DESCRIPTION = DESCRIPTION

# This call to setup() does all the work
setup(
    name="sciunit_convert",
    version=VERSION,
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/shilvi/python2jupyter",
    author="Shilvi Satpati",
    author_email="rimli.satpati@gmail.com",
    python_requires=">=3.6.0",
    license="MIT",
    entry_points={
        'console_scripts': [
            'sciunit_convert=sciunit_convert.convert:main',
            'exec_abs=sciunit_convert.exec_abs:main',
            'nbrunner=sciunit_convert.nbrunner:main',
            'dmaker=sciunit_convert.dmaker:main'
        ],
    },
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6'
    ],
    keywords='convert python jupyter notebook script',
    packages=['sciunit_convert'],
    include_package_data=True,
    install_requires=open(os.path.join(HERE, 'requirements.txt')).readlines()
)
