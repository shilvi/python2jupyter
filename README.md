# sciunit_convert

## Installation

PyPI

```bash
pip install <>
```

Clone this repository and run Python's setup.py

```bash
git clone https://bitbucket.org/shilvi/python2jupyter
python setup.py install
```

or

```bash
pip install git+https://bitbucket.org/shilvi/python2jupyter#egg=sciunit_convert
```


#### Command line usage

To see the command line usage, run `sciunit_convert -h` and you will get something like this:

```txt
usage: sciunit_convert [-h] [-o OUTPUT_FILENAME] input_filename

Convert a Python script to Jupyter notebook

positional arguments:
  input_filename        Python script/notebook to parse

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT_FILENAME, --output_filename OUTPUT_FILENAME
                        Output filename of Python script. If not specified, it will use the filename of the input with
                        '_converted.py' appended.
```
